Dakota Classics has a unique grouping of collectible cars ranging from 1932 Chevy Coupes to a rare set of 1957 Fords to a 1968 Convertible GTO to 2008 Indy Pace Car Corvette and lots in-between. We also have a huge assortment of memorabilia, including a 12 Phillips 66 Gas Station Sign with post and a set of gas pumps with the Phillips 66 glass on top. Pumps are mounted on a cement base.

To see any of our vehicles, please contact us for an appointment. Wed love to show you the cars we currently have. Dakota Classics is located near Sioux Falls, SD in the heart of the Midwest.

Website : https://www.dakotaclassics.net/
